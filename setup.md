# DevSecOps Lab Setup

## GitLab Setup

The lab exercises require a GitLab account. If you do no have an account, you can create a free one by going [here](https://gitlab.com/users/sign_up).

When you create your account you will also need to create a group. Set the group visibility level to `Public`. This will simplify things if the instructors need to assist in debugging on your exercises. 

### Create Your Subgroup

As part of the course you will be forking a handful of git repos. We are going to have you fork them into a subgroup specific to you within the `gtpe-dso-lab` group. 

Here is what you need to do.
1. Put your GitLab username into chat, so that you can be added to `gtpe-dso-lab/YYYY-MM` for your course. You will be setup as a `Maintainer`.
1. Go to the `gtpe-dso-lab/YYYY-MM` group by going to https://gitlab.com/gtpe-dso-lab/YYYY-MM where MM and YYYY are the month and year of the course.
1. Select `New subgroup`. 
1. Set the Group URL such that it ends with your GitLab username - e.g. https://gitlab.com/gtpe-dso-lab/YYYY-MM/burdellg. 
1. Set the visibility to Public. 
1. Select `Create group`.

When you fork repos throughout the labs, fork them to this subgroup that you have created. 

## Gitpod Setup

As part of the exercises, you will need to clone a few git repositories, make edits, and commit/push changes. You can do this with git on your local machine, but to simplify things we recommend using Gitpod. 

For reference, here is the [Gitpod documentation](https://www.gitpod.io/docs/) and here is [Theia documentation](https://www.gitpod.io/docs/ide/). Theia is the IDE used by Gitpod.

To enable Gitpod you will need to:
1. Go to your GitLab profile preferences: https://gitlab.com/-/profile/preferences
2. Enable Gitpod integration by checking the checkbox at the bottom of the page. 

![Gitpod in GitLab](images/gitlab-gitpod.png)

3. Create a Gitpod account by going to https://gitpod.io/login. Select `GitLab`. Once you select `GitLab` you will asked to authorize Gitpod from GitLab.

![Gitpod Authorization](images/gitpod-auth.png)

Press Authorize.

4. Configure the level of access by going to https://gitpod.io/access-control. Check the box for `repository access` and hit `Update`. This is necessary so that you are able to push to GitLab from Gitpod. 

![GitLab Access Control](images/gitlab-access-control.png)

You will have to re-authorize Gitpod. 

5. You are now able to use Gitpod from within GitLab. On any public repo you can select Gitpod by clicking on the __`v`__ next to `WebIDE`. Once you select Gitpod, a `Gitpod` will now appear. 

![Gitpod Launch](images/gitpod-launch.png)

Note: The directions above set-up Gitpod in the free configuration. You will be limited to public repositories and a maximum of 50 hours per month. 
* Here is some documentation 

6. To verify things are setup correctly, go to: 

https://gitpod.io/#https://gitlab.com/gtpe-dso-lab/dso-exercises

You should see the following:

![Lab Exercises](images/gitpod-exercises.png)

### A Few Notes About Gitpod

Workspaces will stop after 30 minutes of inactivity

To restart the workspace, refresh the page or click the `restart` button.

Save your work -- you can lose uncommitted work when a workspace stops
