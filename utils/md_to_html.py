#!/usr/bin/env python
# coding: utf-8

import os
import re
import shutil
from pathlib import Path

from markdown2 import Markdown

# from https://github.com/trentm/python-markdown2/wiki/link-patterns
pattern = (
    r'((([A-Za-z]{3,9}:(?:\/\/)?)'  # scheme
    r'(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+(:\[0-9]+)?'  # user@hostname:port
    r'|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)'  # www.|user@hostname
    r'((?:\/[\+~%\/\.\w\-_]*)?'  # path
    r'\??(?:[\-\+=&;%@\.\w_]*)'  # query parameters
    r'#?(?:[\.\!\/\\\w]*))?)'  # fragment
    r'(?![^<]*?(?:<\/\w+>|\/?>))'  # ignore anchor HTML tags
    r'(?![^\(]*?\))'  # ignore links in brackets (Markdown links and images)
)
link_patterns = [(re.compile(pattern),r'\1')]

markdowner = Markdown(extras=['smarty-pants', 'tables', 'fenced-code-blocks', 'link-patterns'], link_patterns=link_patterns)

md_files = Path('.').glob('*.md')
html_path = Path.cwd() / 'output' / 'html'

html_path.mkdir(exist_ok=True, parents=True)

for md in md_files:
    if md.name == 'README.md':
        continue
    out = markdowner.convert(md.read_text())
    title = [ln for ln in md.read_text().split('\n') if ln.startswith('# ')][0].lstrip('# ')
    style = """
    <!DOCTYPE html><html>
    <head><link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <style>
      body {font-family: 'Roboto', sans-serif;font-size: 12px;}
      table {font-family: 'Roboto', sans-serif;font-size: 12px;}
      code { background-color: #eee;
             color: #D3863A;
             padding: 2px; 
             line-height: 1.4;
             font-family: monospace;}
    </style>
    </head>
    """
    style += f"""<title>{title}</title>
    <body>"""
    end_tag = "</body>"

    out = '\n'.join([style, out, end_tag])

    html_file = html_path / md.name.replace('.md', '.html')
    html_file.write_text(out)

if os.path.exists(r'output/html/images'):
    shutil.rmtree(r'output/html/images')
shutil.copytree(r'images', r'output/html/images')
